const webpack = require("webpack");

module.exports = {
  publicPath: "./",
  //  构建时的输出目录
  outputDir: "",
  //  放置静态资源的目录
  assetsDir: "",
  //  html 的输出路径
  indexPath: "index.html",
  //打包优化
  chainWebpack: (config) => {
    config.devtool(config.mode === "production" ? false : "source-map");
  },
  // 生产环境是否生成 sourceMap 文件
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      alias: {
        components: "@/components",
        content: "components/content",
        common: "components/common",
        assets: "@/assets",
        services: "@/services",
        pages: "@/pages",
      },
    },
  },
  pages: {
    index: {
      // page 的入口
      entry: "src/main.js",
      // entry: 'src/index.html',
      // 模板来源
      //   template: 'public/index.html',
      // 在 dist/index.html 的输出
      //   filename: 'index.html',
      // 当使用 title 选项时，template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: "",
      // 在这个页面中包含的块，默认情况下会包含
      // 提取出来的通用 chunk 和 vendor chunk。
      //   chunks: ['chunk-vendors', 'chunk-common', 'index']
    },
    // 当使用只有入口的字符串格式时，模板会被推导为 `public/subpage.html`，并且如果找不到的话，就回退到 `public/index.html`。
    // 输出文件名会被推导为 `subpage.html`。
    // subpage: 'src/subpage/main.js'
  },

  // webpack-dev-server 相关配置
  devServer: {
    open: true,
    port: 3000,
    https: false,
    hotOnly: false,
    proxy: {
      // 设置代理
      "/api": {
        // target: 'http://192.168.10.176:9016/',
        // target: 'http://192.168.10.95:9000/', //文杰
        // target: 'http://192.168.10.135:9000/', // 测试
        // target: 'http://192.168.10.169:9000/', //壮成
        // target: 'http://192.168.10.154:9016/', //治华
        // target: 'http://192.168.10.169:8097/', //丁鹏
        // target: 'http://10.32.49.40:8010/cockpit', //线上
        // target: 'http://192.168.10.165:9016',
        target: "http://192.168.10.169:8097/",
        changeOrigin: true,
        pathRewrite: {
          "^/api": "",
        },
      },
    },
    before: (app) => {},
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
    externals: {
      AMap: "AMap",
    },
  },

  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "windows.jQuery": "jquery",
      }),
    ],
  },
  // configureWebpack(config) {
  //   //如果有更改devtool的行为，请先判断是否是production环境
  //   //比如这样的 config.devtool="source-map";
  //   //改为下面这样的
  //   config.devtool = config.mode === "production" ? false : "source-map";
  // },
};
