import axios from "axios";
import { message } from "@/utils/Resmessage";

import { BASE_URL, TIMEOUT } from "./config";

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: TIMEOUT,
  withCredentials: true,
});

instance.interceptors.request.use(
  (config) => {
    // 1.发送网络请求时, 在界面的中间位置显示Loading的组件

    // 2.某一些请求要求用户必须携带token, 如果没有携带, 那么直接跳转到登录页面

    // 3.params/data序列化的操作

    return config;
  },
  (err) => {}
);

// 添加响应拦截器
instance.interceptors.response.use(
  (response) => {
    if (response.data && response.data.code == -1) {
      message({
        type: "error",
        message: response.data.message,
      });
      return Promise.reject(response);
    } else if (!Number(response.data.code)) {
      return response;
    } else {
      return response;
    }
  },
  (error) => {
    if (error.response && error.response.status == "500") {
      message({
        type: "error",
        message: "服务器开小差！",
      });

      return;
    }

    return Promise.reject(error);
    // }
  }
);
export default instance;
