# clf-vue-temp

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### 项目创建

自动创建vue2项目——自动安装依赖——自动运行项目——自动打开浏览器

```js
clf create prjName
```

### 新增组件模板

```js
clf addcpn cpnName [-d 新建文件夹位置]可选

//clf addcpn demo, 例如: why addcpn HelloWorld [-d src/components]"
```

### 新增cpn+router

```js
clf addpage demo [-d 新建文件夹位置]可选

//clf add demo, 例如: clf addpage Home [-d src/pages]
```

### 新增store

```js
clf addstore demo [-d 新建文件夹位置]可选

// clf add demo, 例如: clf addstore Home [-d src/store]
```

